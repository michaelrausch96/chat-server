#include <winsock2.h>
#include <Windows.h>
#include <ws2tcpip.h>
#include <fstream>
#include <cstring>
#include <string>
#include <sstream>
#include <ctime>
#include <cerrno>
#include <cassert>

using namespace std;

//Winsock Library
#pragma comment(lib,"ws2_32.lib")

//Modern UI, instead of falling back to 1995
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif


//Window element and winsock messages
#define IDC_EDIT_IN			101
#define IDC_EDIT_OUT		102
#define IDC_SEND_BUTTON		103
#define WM_SOCKET			104
#define INN_PLUGINLOADER    1
#define INN_WEBLOADER		0

//Window Element HWNDs
HWND hIncomingMessageBox = NULL;
HWND hMessageBox = NULL;
HWND hSendMessageBoxLabel = NULL;
HWND hServerHistoryLabel = NULL;

//Server Port
int serverPort = 6482;

//Maximum amount of clients
const int maxClients = 10000;

//Amount of clients connected + global generic socket stuff, maybe move this?
int nClient = 0;
SOCKET Socket[maxClients - 1];
SOCKET ServerSocket = NULL;

//Public BOOLs 
bool vidshare = 0;//Is vidshare active, remove soon when replaced with new plugin loader
bool motdShown = 0;//Has MOTD error been shown

//Unused global string, remove this?...
string userName;

//Array for chat history
char szHistory[10000];

//Clients socket address
sockaddr sockAddrClient;

//Function Prototypes
LRESULT CALLBACK WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

//Function to get current message of the day and return it
string getMotd(){//Use pointers instead?
	ifstream motd;
	motd.open("motd.ini", ios::in | ios::binary);
	if (motd){
		//Read WHOLE motd.ini and return it as a string
		string tempdata;
		motd.seekg(0, ios::end);
		tempdata.resize(motd.tellg());
		motd.seekg(0, ios::beg);
		motd.read(&tempdata[0], tempdata.size());
		motd.close();
		return tempdata;
	}
	else{
		//Only show error message if it has not been shown before
		if (motdShown == 0){
			MessageBox(NULL, "No MOTD?", "Error", MB_ICONERROR);
			motdShown = 1;
		}
		//Defualt MOTD
		return "Welcome to the server!";
	}
}

//Take a string and save it to eventlog.txt
void eventLog(string message){
	ofstream log;
	log.open("eventlog.txt", ios_base::app);
	if (log){//Ensure it opened successfully before writing
		log << message << endl;
		log.close();
	}
	else{
		//Display error if not open
		MessageBox(NULL, "Cannot open log for writing", "Error", MB_ICONERROR);
	}
}

//Convert int to string
string convertInt(int num)
{
	//create a stringstream add number and return
	stringstream ss;
	ss << num;
	string temp = ss.str();
	return temp;
}

//Save message to tempdata
void externalCommander(char* message, int web=0){
	if (web == 0){
		ofstream a_file("tempdata");
		a_file << message;
		a_file.close();
	}
	else{
		ofstream a_file("webdata");
		a_file << message;
		a_file.close();
	}
}

void parseHTML(char* message){
	//Convert char* to string
	string msg = string(message);

	//Verify that its a HTML request
	size_t isGET = msg.find("GET");
	size_t isKEEPALIVE = msg.find("keep-alive");
	size_t isTEXTHTML = msg.find("text/html");

	//Commands
	size_t shutdown = msg.find("alert");

	//Check if everything is ok, then check command
	if (isGET != std::string::npos && isKEEPALIVE != std::string::npos && isTEXTHTML != std::string::npos){
		if (shutdown != std::string::npos){
			//Runs if GET was "alert"
			
			externalCommander("asdfghjkl");
		}
	}
}

//Process messages, and check if they match an internal command
void internalCmdProc(char* message){
	//Open cmdlog.txt for writing
	ofstream cmdlog;
	cmdlog.open("cmdlog.txt", ios_base::app);

	if (cmdlog){
		//Convert char* to string
		string msg(message);

		//Check message
		if (vidshare == 1){
			//Vidshare is redundant, remove in next version when using ICLP plugin loader
			vidshare = 0;
			string URL = "start vidshare " + msg;
			char *a = new char[URL.size() + 1];
			a[URL.size()] = 0;
			memcpy(a, URL.c_str(), URL.size());
			system(a);
		}
		//Test command
		if (msg == "COMMAND TEST"){
			MessageBox(NULL, "this is an internal CMD", "Info!", MB_ICONINFORMATION);
		}
		else if (msg == "sharevideo "){
			vidshare = 1;
		}
	}
	cmdlog.close();
}

//Save chat messages to log
void saveLog(string message){
	//open chatlog.txt for writing
	ofstream log;
	log.open("chatLog.txt", ios_base::app);
	if (log){
		log << message << endl;
		log.close();
	}
	else{
		//Give error if it cannot be opened
		MessageBox(NULL, "Cannot open log for writing", "Error", MB_ICONERROR);
	}
}

//Repeats a message recieved to ALL clients, maybe add a PM feature
void messageRepeat(char* message){
	//Convert char* message into a string, save it into log file
	string messageString(message);
	saveLog(messageString);

	//add SHOUT>> to front of messagestring, will be changed to user ID later(DONE)
	//Keeping it just incase
	messageString = "" + messageString;

	//string to char*
	char *fmsg = new char[messageString.size() + 1];
	fmsg[messageString.size()] = 0;
	memcpy(fmsg, messageString.c_str(), messageString.size());

	//Send to each client
	for (int n = 0; n <= nClient; n++){
		send(Socket[n], fmsg, strlen(fmsg), 0);
	}
	SendMessage(hMessageBox, WM_SETTEXT, NULL, (LPARAM)"");
}

//Main function, using win32Api
int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nShowCmd)
{
	//Create window class
	WNDCLASSEX wClass;
	ZeroMemory(&wClass, sizeof(WNDCLASSEX));
	wClass.cbClsExtra = NULL;
	wClass.cbSize = sizeof(WNDCLASSEX);
	wClass.cbWndExtra = NULL;
	wClass.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wClass.hIcon = NULL;
	wClass.hIconSm = NULL;
	wClass.hInstance = hInst;
	wClass.lpfnWndProc = (WNDPROC)WinProc;
	wClass.lpszClassName = "winclass";
	wClass.lpszMenuName = NULL;
	wClass.style = CS_HREDRAW | CS_VREDRAW;

	//If registering window class failed, give error
	if (!RegisterClassEx(&wClass)){
		int nResult = GetLastError();
		MessageBox(NULL,
			"Cant create window class\r\nError code:",
			"win class fail",
			MB_ICONERROR);
	}

	//Create window
	HWND hWnd = CreateWindowEx(NULL,
		"winclass",
		"chatthing.avi",
		WS_OVERLAPPED | WS_MINIMIZEBOX | WS_SYSMENU,
		200, 200, 640, 400,
		NULL, NULL,
		hInst,
		NULL);

	//If window creation failed, give error, then show window
	if (!hWnd){
		int nResult = GetLastError();
		MessageBox(NULL,
			"Cant create window\r\nError code:",
			"cant create window",
			MB_ICONERROR);
	}
	ShowWindow(hWnd, nShowCmd);

	//Create message variable, then zero the memory
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	//Main message loop
	while (GetMessage(&msg, NULL, 0, 0)){
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 0;
}

LRESULT CALLBACK WinProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg){
	case WM_COMMAND:
		switch (LOWORD(wParam)){
			//Send button
		case IDC_SEND_BUTTON:{
								 //Create buffer and zero memory
								 char szBuffer[1024];
								 ZeroMemory(szBuffer, sizeof(szBuffer));

								 //Get text from message box
								 SendMessage(hMessageBox,
									 WM_GETTEXT,
									 sizeof(szBuffer),
									 reinterpret_cast<LPARAM>(szBuffer));

								 //Put SERVER>> at beginning of message so user 
								 //knows its from the server
								 string messageFromServer(szBuffer);
								 string tmpbuffer = "SERVER >> " + messageFromServer;
								 char *sendToCl = new char[tmpbuffer.size() + 1];
								 sendToCl[tmpbuffer.size()] = 0;
								 memcpy(sendToCl, tmpbuffer.c_str(), tmpbuffer.size());

								 //Send to each client
								 for (int n = 0; n <= nClient; n++){
									 send(Socket[n], sendToCl, strlen(sendToCl), 0);
								 }
								 //Empty message box
								 SendMessage(hMessageBox, WM_SETTEXT, NULL, (LPARAM)"");
		}
			break;
		}
		break;
	case WM_CREATE:{
					   //RUN ON WINDOW CREATION

					   //Clear the history
					   ZeroMemory(szHistory, sizeof(szHistory));

					   //incoming message box
					   hIncomingMessageBox = CreateWindowEx(WS_EX_CLIENTEDGE,
						   "EDIT", "",
						   WS_CHILD | WS_VISIBLE | ES_MULTILINE |
						   ES_AUTOVSCROLL | ES_AUTOHSCROLL,
						   170, 130, 400, 200,
						   hWnd,
						   (HMENU)IDC_EDIT_IN,
						   GetModuleHandle(NULL),
						   NULL);

					   //Send message box label
					   hSendMessageBoxLabel = CreateWindowEx(0,
						   "STATIC", "Enter Text here:",
						   WS_CHILD | WS_VISIBLE,
						   50, 10, 120, 20, hWnd,
						   NULL, GetModuleHandle(NULL),
						   NULL);

					   //Server history box label
					   hServerHistoryLabel = CreateWindowEx(0,
						   "STATIC", "Server Log:", WS_CHILD | WS_VISIBLE,
						   50, 130, 120, 20, hWnd, NULL, GetModuleHandle(NULL), NULL);

					   //if incoming message box failed to be created give error
					   if (!hIncomingMessageBox){
						   MessageBox(hWnd,
							   "can't create incoming message box.",
							   "Error",
							   MB_OK | MB_ICONERROR);
					   }

					   //Set Default font
					   HGDIOBJ hDefaultFont = GetStockObject(DEFAULT_GUI_FONT);
					   SendMessage(hIncomingMessageBox,
						   WM_SETFONT,
						   (WPARAM)hDefaultFont,
						   MAKELPARAM(FALSE, 0));

					   //Clear incoming message box
					   SendMessage(hIncomingMessageBox,
						   WM_SETTEXT,
						   NULL,
						   (LPARAM)" ");

					   // Create outgoing message box
					   hMessageBox = CreateWindowEx(WS_EX_CLIENTEDGE,
						   "EDIT",
						   "",
						   WS_CHILD | WS_VISIBLE | ES_MULTILINE |
						   ES_AUTOVSCROLL | ES_AUTOHSCROLL,
						   170, 10, 400, 60,
						   hWnd,
						   (HMENU)IDC_EDIT_IN,
						   GetModuleHandle(NULL),
						   NULL);

					   //if outgoing message box failed to be created give error
					   if (!hMessageBox){
						   MessageBox(hWnd,
							   "Could not create outgoing edit box.",
							   "Error",
							   MB_OK | MB_ICONERROR);
					   }

					   //Set font for message box
					   SendMessage(hMessageBox,
						   WM_SETFONT,
						   (WPARAM)hDefaultFont,
						   MAKELPARAM(FALSE, 0));

					   //Clear message box
					   SendMessage(hMessageBox,
						   WM_SETTEXT,
						   NULL,
						   (LPARAM)" ");

					   // Create send button
					   HWND hWndSendButton = CreateWindow("BUTTON", "Send",
						   WS_TABSTOP | WS_VISIBLE |
						   WS_CHILD | BS_DEFPUSHBUTTON,
						   170, 80, 75, 23,
						   hWnd,
						   (HMENU)IDC_SEND_BUTTON,
						   GetModuleHandle(NULL),
						   NULL);

					   //Set font for send button
					   SendMessage(hWndSendButton,
						   WM_SETFONT,
						   (WPARAM)hDefaultFont,
						   MAKELPARAM(FALSE, 0));

					   //Write successful server start to log
					   string serverStartErrorText = "New server started, the current server time is: " + convertInt(time(0));
					   eventLog(serverStartErrorText);

					   //Start winsock
					   WSADATA wsaData;
					   int nResult = WSAStartup(MAKEWORD(2, 2), &wsaData);

					   //Display error if failed
					   if (nResult != 0){
						   MessageBox(hWnd,
							   "Winsock initialization failed",
							   "Critical Error",
							   MB_ICONERROR);
						   SendMessage(hWnd, WM_DESTROY, NULL, NULL);
						   break;
					   }

					   //Create Socket, display error if failed
					   ServerSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

					   if (ServerSocket == INVALID_SOCKET){
						   MessageBox(hWnd,
							   "Socket creation failed",
							   "Critical Error",
							   MB_ICONERROR);
						   SendMessage(hWnd, WM_DESTROY, NULL, NULL);
						   break;
					   }


					   SOCKADDR_IN SockAddr;
					   SockAddr.sin_port = htons(serverPort);
					   SockAddr.sin_family = AF_INET;
					   SockAddr.sin_addr.s_addr = htonl(INADDR_ANY);

					   //if unable to bind socket, show error
					   if (bind(ServerSocket, (LPSOCKADDR)&SockAddr, sizeof(SockAddr)) == SOCKET_ERROR){
						   MessageBox(hWnd,
							   "Unable to bind socket",
							   "Error", MB_OK);

						   SendMessage(hWnd, WM_DESTROY, NULL, NULL);
						   break;
					   }


					   nResult = WSAAsyncSelect(ServerSocket,
						   hWnd,
						   WM_SOCKET,
						   (FD_CLOSE | FD_ACCEPT | FD_READ));
					   if (nResult){
						   MessageBox(hWnd,
							   "WSAAsyncSelect failed",
							   "Critical Error",
							   MB_ICONERROR);
						   SendMessage(hWnd, WM_DESTROY, NULL, NULL);
						   break;
					   }

					   if (listen(ServerSocket, SOMAXCONN) == SOCKET_ERROR){
						   MessageBox(hWnd,
							   "Unable to listen!",
							   "Error", MB_OK);
						   SendMessage(hWnd, WM_DESTROY, NULL, NULL);
						   break;
					   }
	}
		break;

	case WM_DESTROY:
	{
					   PostQuitMessage(0);
					   shutdown(ServerSocket, SD_BOTH);
					   closesocket(ServerSocket);
					   WSACleanup();
					   return 0;
	}
		break;

	case WM_SOCKET:
	{
					  switch (WSAGETSELECTEVENT(lParam))
					  {
					  case FD_READ:
					  {
									  for (int n = 0; n <= maxClients; n++){
										  char szIncoming[1024];
										  ZeroMemory(szIncoming, sizeof(szIncoming));

										  int inDataLength = recv(Socket[n],
											  (char*)szIncoming,
											  sizeof(szIncoming) / sizeof(szIncoming[0]),
											  0);

										  if (inDataLength != -1){
											  strncat_s(szHistory, szIncoming, inDataLength);
											  strcat_s(szHistory, "\r\n");
											  messageRepeat(szIncoming);

											  

											  internalCmdProc(szIncoming);

											  externalCommander(szIncoming);
											  //HTML PARSER
											  parseHTML(szIncoming);

											  SendMessage(hIncomingMessageBox,
												  WM_SETTEXT,
												  sizeof(szIncoming)-1,
												  reinterpret_cast<LPARAM>(&szHistory));
										  }
									  }
					  }
						  break;

					  case FD_CLOSE:
					  {
									   SendMessage(hIncomingMessageBox,
										   WM_SETTEXT,
										   NULL,
										   (LPARAM)"Client closed connection!");
					  }
						  break;

					  case FD_ACCEPT:
					  {
										if (nClient<maxClients){
											int size = sizeof(sockaddr);
											Socket[nClient] = accept(wParam, &sockAddrClient, &size);

											if (Socket[nClient] == INVALID_SOCKET){
												int nret = WSAGetLastError();
												WSACleanup();
											}

											string clientLoginText = "Client #" + convertInt(nClient+1) + " Connected out of " + convertInt(maxClients) + ". The current time is " + convertInt(time(0)) + "\n";
											eventLog(clientLoginText);

											string motdTemp = getMotd();

											char *a = new char[motdTemp.size() + 1];
											a[motdTemp.size()] = 0;
											memcpy(a, motdTemp.c_str(), motdTemp.size());

											send(Socket[nClient], a, strlen(a), 0);

											SendMessage(hIncomingMessageBox,
												WM_SETTEXT,
												NULL,
												(LPARAM)"Client connected!");
										}
										nClient++;
					  }
						  break;
					  }
	}
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}